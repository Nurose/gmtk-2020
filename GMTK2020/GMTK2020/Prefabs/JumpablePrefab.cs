﻿using Nurose.Core;

namespace GMTK2020
{
    public static class JumpablePrefab
    {
        public static void Create(World world, Vector2 pos)
        {
            var e = new Entity(world);
            e.AddComponent(new Transform() { LocalPosition = pos });
            e.AddComponent(new BoxColliderComponent() { IsStatic = true, Size = new Vector2(1,.7f)});
            e.AddComponent(new JumpableComponent() { });

            e.AddComponent(new TextureRenderData()
            {
                Texture = world.ResourceManager.Get<Texture>("lowbushTexture"),
                Tint = Color.White,
            });
        }
    }
}
