﻿using Nurose.Core;

namespace GMTK2020
{
    public static class ParticlePrefab
    {
        public static void Create(World world, Vector2Int pos)
        {
            var e = new Entity(world);
            e.AddComponent(new Transform() { LocalPosition = pos, LocalSize = Vector2.One * Utils.RandomFloat(10,20)});
            Vector2 dir = -((Vector2)(pos - NuroseMain.Window.Size / 2)).Normalized;
             dir =Utils.DegreeToVector( Utils.VectorToDegree(dir) + Utils.RandomFloat(-80, 80));
            e.AddComponent(new Particle() { Velocity = dir*1500, Color = Utils.RandomColor(.7f) });

        }
    }
}
