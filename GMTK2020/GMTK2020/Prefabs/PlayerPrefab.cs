﻿using Nurose.Core;
using System.Collections.Generic;

namespace GMTK2020
{
    public static class PlayerPrefab
    {
        public static void Create(World world)
        {
            var e = new Entity(world);
            var t = e.AddComponent<Transform>();
            t.LocalSize = Vector2.One * 1.3f;
            t.LocalPosition = HorseData.SpawnPos;
            e.AddComponent(new HorseData()
            {
                MovementKeys = new LearnedKey[]
                {
                    new LearnedKey(Key.W),
                    new LearnedKey(Key.A),
                    new LearnedKey(Key.S),
                    new LearnedKey(Key.D),
                },
                JumpKey = new LearnedKey(Key.Space)
            });
            e.AddComponent<PhysicsData>();
            e.AddComponent<ShooterData>();
            e.AddComponent(new BoxColliderComponent() { offset = new Vector2(1,1)*.25f, Size = Vector2.One * .5f });

            var textureNameBase = "Resources/Images/horse/paard000";
            e.AddComponent(new MultiWayTextureComponent()
            {
                Entries = new List<MultiWayTextureComponent.Entry>()
                {
                    new MultiWayTextureComponent.Entry(Vector2.Left, new Texture(textureNameBase +"5" + ".png")),
                    new MultiWayTextureComponent.Entry(Vector2.Right, new Texture(textureNameBase +"1" + ".png")),
                    new MultiWayTextureComponent.Entry(Vector2.Up, new Texture(textureNameBase +"7" + ".png")),
                    new MultiWayTextureComponent.Entry(Vector2.Down, new Texture(textureNameBase +"3" + ".png")),
                    new MultiWayTextureComponent.Entry(Vector2.Right + Vector2.Up, new Texture(textureNameBase +"8" + ".png")),
                    new MultiWayTextureComponent.Entry(Vector2.Right + Vector2.Down, new Texture(textureNameBase +"2" + ".png")),
                    new MultiWayTextureComponent.Entry(Vector2.Left + Vector2.Up, new Texture(textureNameBase +"6" + ".png")),
                    new MultiWayTextureComponent.Entry(Vector2.Left + Vector2.Down, new Texture(textureNameBase +"4" + ".png"))
                }
            });
        }
    }
}
