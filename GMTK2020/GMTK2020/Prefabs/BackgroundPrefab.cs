﻿using Nurose.Core;

namespace GMTK2020
{
    public static class BackgroundPrefab
    {
        public static void Create(World world)
        {
            var e = new Entity(world);
            e.AddComponent(new CollidableBackground()
            {
                Texture = world.ResourceManager.Get<Texture>("mainbackgroundTexture")
            });
            e.AddComponent(new TextureRenderData()
            {
                Texture = world.ResourceManager.Get<Texture>("mainbackgroundTexture")
            });
            e.AddComponent(new Transform()
            {
                LocalSize = new Vector2(1500, 1000) / GameSize.PixelsPerUnit
            }); ;

        }
    }
}
