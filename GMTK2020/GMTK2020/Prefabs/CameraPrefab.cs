﻿using Nurose.Core;

namespace GMTK2020
{
    public static class CameraPrefab
    {
        public static void Create(World world)
        {
            var e = new Entity(world);
            e.AddComponent(new Transform() { LocalSize = Vector2.One * 48});
            e.AddComponent(new Camera() { PixelsPerUnit = GameSize.PixelsPerUnit });
        }
    }
}
