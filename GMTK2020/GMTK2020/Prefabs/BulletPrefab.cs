﻿using Nurose.Core;
using System.Collections.Generic;
using System.Linq;

namespace GMTK2020
{
    public static class BulletPrefab
    {
        public static void Create(World world, Vector2 pos, Vector2 target)
        {
            Vector2 size = Vector2.One * 0.25f;
            Vector2 diff = (target - pos);

            float inaccuracy = Utils.Min(1, 0.5f * diff.SquaredMagnitude);
            //target += Utils.RandomPointInUnitCircle() * inaccuracy;

            Vector2 dir = diff.Normalized;
            var e = new Entity(world);
            e.AddComponent(new Transform() { LocalPosition = pos - size / 2, LocalSize = size, LocalRotation = Utils.RandomFloat(0, 360) });
            e.AddComponent(new BulletComponent() { Target = target, StartDirection = dir });
            e.AddComponent(new PhysicsData() { Velocity = (target - pos) * .1f });


            world.Components.UpdateContent();

            var ignorelist = new List<BoxColliderComponent>();
            ignorelist.AddRange(world.Components.GetByFamily<MovementSystem.HorseFam>().Select(s => s.Collider));
            ignorelist.AddRange(world.Components.GetByFamily<MovementSystem.JumpableFam>().Select(s => s.Collider));

            e.AddComponent(new BoxColliderComponent()
            {
                ToIgnore = new HashSet<BoxColliderComponent>(ignorelist)
            });

            e.AddComponent(new TextureRenderData()
            {
                Texture = world.ResourceManager.Get<Texture>("sugarCubeTexture"),
                Tint = Color.White,
            });
        }
    }
}
