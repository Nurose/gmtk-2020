﻿using Nurose.Core;

namespace GMTK2020
{
    public static class HiddenWallPrefab
    {
        public static void Create(World world, Vector2 pos)
        {
            var e = new Entity(world);
            e.AddComponent(new Transform() { LocalPosition = pos, LocalSize = Vector2.One / 2 }) ;
            e.AddComponent(new BoxColliderComponent() { IsStatic = true });
            e.AddComponent(new ResetOnCollisionComponent() {});
        }
    }
}
