﻿using Nurose.Core;

namespace GMTK2020
{
    public static class BushPrefab
    {
        public static void Create(World world, Vector2 pos)
        {
            var e = new Entity(world);
            e.AddComponent(new Transform() { LocalPosition = pos});
            e.AddComponent(new BoxColliderComponent() { IsStatic = true });

            e.AddComponent(new TextureRenderData()
            {
                Texture = world.ResourceManager.Get<Texture>("bushTexture"),
                Tint = Color.White,
            });
        }
    }
}
