﻿using Nurose.Core;

namespace GMTK2020
{
    public static class PawnPrefab
    {
        public static void Create(World world, Vector2 pos)
        {
            var e = new Entity(world);
            e.AddComponent(new Pawn() { OriginalPosition = pos });
            e.AddComponent(new Transform() { LocalPosition = pos, LocalSize = Vector2.One/1.4f });
            e.AddComponent(new BoxColliderComponent() { Size = new Vector2(1,.8f) });

            e.AddComponent(new TextureRenderData()
            {
                Texture = world.ResourceManager.Get<Texture>("pawnTexture"),
                Tint = Color.White,
            });
        }
    }
}
