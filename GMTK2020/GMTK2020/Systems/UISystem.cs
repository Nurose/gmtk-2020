﻿using Nurose.Core;
using Nurose.Text;
using System.Linq;

namespace GMTK2020
{
    public class UISystem : WorldSystem
    {

        int heightPer = 60;
        int marginPer = 10;
        int maxWidth = 100;

        public bool Help = false;

        string[] lines;
        public void Start()
        {
            lines = new string[]
            {
                "HELP SCREEN",
                "",
                "You just got your new horse and he is out of control!",
                "Teach your horse to follow your directions by holding the",
                "movement (WASD) keys while the horse is getting its food.",
                "This will associate the movement with the key.",
                "Teach it to jump by holding the SPACE key while he is jumping.",
                "Shoot apples by left clicking in the starting area.",
                "",
                "When you think he is tamed enough you can try",
                "completing the obstacles course. (No apples there)"
            };
        }

        public void Draw()
        {
            Drawer.StartPixelSpace();
            Vector2Int pos = new Vector2Int(20, 10);

            var moveAverage = World.Components.Get<HorseData>().MovementKeys.Average(d => d.Certainty);
            var jumpProgress = World.Components.Get<HorseData>().JumpKey.Certainty;

            DrawProgression(pos, moveAverage, "wasd");
            pos += (0, heightPer + marginPer);
            DrawProgression(pos, jumpProgress, "space");

            Drawer.EndPixelSpace();
            if (Help)
            {
                Drawer.StartPixelSpace();
                Drawer.DrawRect(Vector2Int.Zero, NuroseMain.Window.Size, new Color(0,0,0,.9f), .1f);
                int lineHeight = 30;
                int y = 0;

                foreach (var line in lines)
                {
                    Drawer.DrawText(line, (4, y), lineHeight, Color.White, 0.01f, ResourceManager, "cascadia");
                    y += lineHeight;
                }

                Drawer.EndPixelSpace();

            }

            if (World.Systems.Get<GameResetSystem>().Won)
            {
                Drawer.StartPixelSpace();
                Drawer.DrawText("You won!", new Vector2Int(World.RenderTarget.Size.X / 2, 200), 100 + (int)(Utils.Sin(Time.SecondsSinceStart*10)*10), Color.White, 0.1f, ResourceManager, "calibri", true);
                Drawer.EndPixelSpace();
            }
        }

        public void FixedUpdate()
        {
            if (Input.IsKeyPressed(Key.F1))
                Help = !Help;
        }

        private void DrawProgression(Vector2Int pos, float average, string textureResourceName)
        {
            Drawer.DrawRect(pos, Vector2.One * heightPer, Color.White, .1f, World.ResourceManager.Get<Texture>(textureResourceName));
            pos += new Vector2Int(heightPer, 15);
            Drawer.DrawRect(pos, new Vector2(maxWidth, heightPer/2), Color.Grey(.5f), .1f);
            Drawer.DrawRect(pos, new Vector2(maxWidth * average, heightPer/2), Color.Blue, .1f);
        }
    }
}
