﻿using Nurose.Core;
using System;

namespace GMTK2020
{
    public class GameResetSystem : WorldSystem
    {

        public bool Won = false;
        public struct PawnFam
        {
            public Transform Transform;
            public Pawn Pawn;
        }

        public void FixedUpdate()
        {
            foreach (var fam in World.Components.GetByFamily<PawnFam>())
            {
                if(Utils.Distance(fam.Pawn.OriginalPosition, fam.Transform.LocalPosition)  > .3f)
                {
                    ResetWorld();
                }
            }

            HorseData horse = World.Components.Get<HorseData>();
            var horseId = horse.EntityID;
            var trans = World.Components.Get<Transform>(horseId);

            if (Utils.Distance(trans.GlobalPosition, (27f, 4.1f)) < 2 )
            {
                Win(horse);
            }
            else if( Utils.Distance(trans.GlobalPosition, (27f, 4.1f)) > 4 )
            {
                Won = false;
            }

            if (Input.IsKeyPressed(Key.R))
                ResetWorld();
        }

        private void Win(HorseData horse)
        {
            Won = true;
            for (int i = 0; i < 3; i++)
            {

            ParticlePrefab.Create(World,NuroseMain.Window.Size);
            ParticlePrefab.Create(World, (-50,NuroseMain.Window.Height) );
            }
        }

        public void ResetWorld()
        {
            HorseData horse = World.Components.Get<HorseData>();
            var horseId = horse.EntityID;
            var trans = World.Components.Get<Transform>(horseId);
            trans.LocalPosition = HorseData.SpawnPos;
            horse.IdleTarget = null;

            foreach (var fam in World.Components.GetByFamily<PawnFam>())
            {
                fam.Transform.LocalPosition = fam.Pawn.OriginalPosition;
            }


            foreach (var fam in World.Components.GetAll<BulletComponent>())
            {
                World.Components.RemoveAllOf(fam.EntityID);
            }
        }
    }
}
