﻿using Nurose.Core;

namespace GMTK2020
{
    public class CameraSystem : WorldSystem
    {
        public void Start()
        {

        }

        public void Draw()
        {
            if (World.Systems.Get<UISystem>().Help)
                return;

            var cam = World.Components.Get<Camera>();
            var camTrans = World.Components.Get<Transform>(cam.EntityID);

            var target = World.Components.Get<Transform>(World.Components.Get<HorseData>().EntityID);

            if (Utils.Distance(camTrans.LocalPosition, target.LocalCenterPoint) > 10)
                camTrans.LocalPosition = target.LocalCenterPoint;
            else
                camTrans.LocalPosition = Utils.Lerp(camTrans.LocalPosition, target.LocalCenterPoint, Time.DeltaTime*7);

            Drawer.SetCameraMatrix(new TransformData(camTrans.LocalPosition, camTrans.LocalSize * 2).CalcViewMatrix());
        }
    }
}
