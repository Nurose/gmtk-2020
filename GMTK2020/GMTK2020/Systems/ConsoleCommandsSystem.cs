﻿using Nurose.Console;
using Nurose.Core;
using System;
using System.Diagnostics;

namespace GMTK2020
{
    public class ConsoleCommandsSystem : WorldSystem
    {
        bool done = false;
        private Stopwatch stopwatch;
        private int i = 0;

        public void Start()
        {
            stopwatch = new Stopwatch();
            stopwatch.Start();
            NuroseMain.Window.VSync = true;
        }


        [Command]
        public void Tame()
        {
            float certainty = .96f;
            HorseData horseData = World.Components.Get<HorseData>();
            horseData.MovementKeys = new LearnedKey[]
            {
                new LearnedKey(Key.W){ Certainty = certainty, Direction = Vector2.Up},
                new LearnedKey(Key.A){ Certainty = certainty, Direction = Vector2.Left},
                new LearnedKey(Key.S){ Certainty = certainty, Direction = Vector2.Down},
                new LearnedKey(Key.D){ Certainty = certainty, Direction = Vector2.Right},
            };
            horseData.JumpKey = new LearnedKey(Key.Space) { Certainty = certainty };
        }

        [Command]
        public void UnTame()
        {
            HorseData horseData = World.Components.Get<HorseData>();
            horseData.MovementKeys = new LearnedKey[]
            {
                new LearnedKey(Key.W){ Certainty = 0, Direction = default},
                new LearnedKey(Key.A){ Certainty = 0, Direction = default },
                new LearnedKey(Key.S){ Certainty = 0, Direction = default},
                new LearnedKey(Key.D){ Certainty = 0, Direction = default},
            };
            horseData.JumpKey = new LearnedKey(Key.Space) { Certainty = 0 };
        }

        public void Draw()
        {
            i++;
            if (stopwatch.ElapsedMilliseconds > 1000)
            {
                Logger.LogAnnouncement($"FPS={Math.Round(i / stopwatch.Elapsed.TotalSeconds)}");
                stopwatch.Restart();
                i = 0;
            }
        }
        }
}
