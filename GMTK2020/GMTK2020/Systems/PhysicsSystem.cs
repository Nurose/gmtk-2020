﻿using Nurose.Core;

namespace GMTK2020
{
    public class PhysicsSystem : WorldSystem
    {
        public struct Fam
        {
            public Transform Transform;
            public PhysicsData Rigid;
        }

        public void FixedUpdate()
        {
            if (World.Systems.Get<UISystem>().Help)
                return;

            foreach (var fam in World.Components.GetByFamily<Fam>())
            {
                fam.Rigid.Velocity = Utils.Lerp(fam.Rigid.Velocity, fam.Rigid.ForceToAdd,Time.FixedDeltaTime*8);
                
                fam.Transform.LocalPosition += fam.Rigid.Velocity;
               //fam.Rigid.Velocity *= .9f;
                fam.Rigid.ForceToAdd = default;


            }
        }
    }
}
