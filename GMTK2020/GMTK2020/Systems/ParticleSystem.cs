﻿using Nurose.Core;

namespace GMTK2020
{
    public class ParticleSystem : WorldSystem
    {
        public struct ParticleFam
        {
            public Transform Transform;
            public Particle Particle;
        }

        public void Draw()
        {
            foreach (var fam in World.Components.GetByFamily<ParticleFam>())
            {
                fam.Transform.LocalRotation += Time.DeltaTime * 500;
                fam.Transform.LocalPosition += fam.Particle.Velocity * Time.DeltaTime;
                fam.Particle.Velocity += new Vector2(0, 1400 * Time.DeltaTime);
                fam.Particle.Velocity = Utils.Lerp(fam.Particle.Velocity, new Vector2(0,0), Time.DeltaTime*1);
                fam.Particle.TimeAlive += Time.DeltaTime;
                if (fam.Particle.TimeAlive > 4)
                {
                    World.Components.RemoveAllOf(fam.Particle.EntityID);
                }

            }
        }
    }
}
