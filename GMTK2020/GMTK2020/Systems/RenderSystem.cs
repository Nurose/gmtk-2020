﻿using Nurose.Core;
using System.Linq;

namespace GMTK2020
{
    public class RenderSystem : WorldSystem
    {
        public UnitRectangleDrawTask drawTask = new UnitRectangleDrawTask(Color.White, 1);

        public struct TextureFam
        {
            public Transform Transform;
            public TextureRenderData TextureRenderer;
        }


        public struct ParticleFam
        {
            public Transform Transform;
            public Particle Particle;
        }

        public struct EightWayTextureFam
        {
            public Transform Transform;
            public MultiWayTextureComponent EightWayTexture;
            public HorseData HorseData;
        }




        public void Draw()
        {


            foreach (var fam in World.Components.GetByFamily<TextureFam>())
            {
                Drawer.SetTexture(fam.TextureRenderer.Texture);
                Drawer.Draw((rt) => rt.SetCurrentShaderUniform("colorTint", fam.TextureRenderer.Tint));
                Drawer.SetModelMatrix(new TransformData(fam.Transform.GlobalPosition, fam.Transform.LocalSize, fam.Transform.LocalRotation).CalcModelMatrix());
                Drawer.Draw(drawTask);
            }



            foreach (var fam in World.Components.GetByFamily<EightWayTextureFam>())
            {
                if (fam.EightWayTexture.Current == null || Utils.Distance(fam.EightWayTexture.Current.dir, fam.HorseData.Direction) > .5f)
                {
                    var newEntry = fam.EightWayTexture.Entries.OrderBy(e => Utils.Distance(e.dir, fam.HorseData.Direction)).First();
                    fam.EightWayTexture.Current = newEntry;
                }
                CalculateJumpOffsetAndScaler(fam, out Vector2 offset, out Vector2 scaler);

                Drawer.SetTexture(fam.EightWayTexture.Current.Texture);
                Drawer.Draw((rt) => rt.SetCurrentShaderUniform("colorTint", Color.White)); ;
                Drawer.SetModelMatrix(new TransformData(fam.Transform.GlobalPosition + offset, fam.Transform.LocalSize * scaler, fam.Transform.LocalRotation).CalcModelMatrix());
                Drawer.Draw(drawTask);
            }


            Drawer.StartPixelSpace();
            foreach (var fam in World.Components.GetByFamily<ParticleFam>())
            {
                Drawer.SetTexture(Texture.White1x1);
                Drawer.Draw((rt) => rt.SetCurrentShaderUniform("colorTint", fam.Particle.Color));
                Drawer.SetModelMatrix(new TransformData(fam.Transform.LocalCenterPoint, fam.Transform.LocalSize, fam.Transform.LocalRotation).CalcModelMatrix());
                Drawer.Draw(drawTask);
            }
            Drawer.EndPixelSpace();
            Drawer.Draw((rt) => rt.SetCurrentShaderUniform("colorTint", Color.White));

        }

        private static void CalculateJumpOffsetAndScaler(EightWayTextureFam fam, out Vector2 offset, out Vector2 scaler)
        {
            float jumpProgres = fam.HorseData.TimeJumping / HorseData.JumpTime;
            float sinProgress = Utils.Sin(jumpProgres * Utils.Pi);

            offset = (0, sinProgress / 2);
            float sizeUp = .3f;
            scaler = Vector2.One * (1 + sinProgress * sizeUp);
            offset -= Vector2.One * (sinProgress * (sizeUp / 2));
        }
    }
}
