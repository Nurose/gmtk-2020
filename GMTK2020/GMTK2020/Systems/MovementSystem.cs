﻿using Nurose.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GMTK2020
{
    public class MovementSystem : WorldSystem
    {
        public struct HorseFam
        {
            public Transform Transform;
            public PhysicsData Physics;
            public BoxColliderComponent Collider;
            public HorseData Horse;
        }

        public struct JumpableFam
        {
            public Transform Transform;
            public BoxColliderComponent Collider;
            public JumpableComponent Jumpable;
        }

        public struct HiddenWallFam
        {
            public Transform Transform;
            public BoxColliderComponent Collider;
            public ResetOnCollisionComponent ResetOnCollision;
        }

        public struct BulletFam
        {
            public Transform Transform;
            public PhysicsData Physics;
            public BulletComponent Bullet;
            public TextureRenderData TextureRender;
        }

        public void FixedUpdate()
        {
            if (World.Systems.Get<UISystem>().Help)
                return;

            foreach (var fam in World.Components.GetByFamily<HorseFam>())
            {
                HorseData horse = fam.Horse;
                var oldState = horse.State;
                switch (horse.State)
                {
                    case HorseState.Neutral:
                        Neutral(fam, horse);
                        break;
                    case HorseState.Tamed:
                        Tamed(fam, horse);
                        break;
                    case HorseState.Excited:
                        MoveTowardsSugarCube(fam);
                        break;
                    default:
                        break;
                }
                if (oldState != horse.State)
                    horse.TimeInCurrentState = 0;

                if (horse.IsJumping)
                {

                    if (horse.TimeJumping > HorseData.JumpTime)
                    {
                        StopJump(fam);
                    }

                    horse.TimeJumping += Time.FixedDeltaTime;

                    if (CheckCollidingWithJumpable(fam) && horse.TimeJumping > HorseData.ExactTimeOnThing)
                    {
                        horse.TimeJumping = HorseData.ExactTimeOnThing;
                    }

                }
                else
                {
                    if (!horse.IsJumping && CheckJumpNeeded(fam) && horse.State != HorseState.Tamed)
                    {
                        if (!(horse.State == HorseState.Neutral && horse.TimeInCurrentState < Time.FixedDeltaTime * 5))
                        {
                            Jump(fam);
                        }
                    }
                }

                horse.TimeInCurrentState += Time.FixedDeltaTime;

                if(World.Systems.Get<GameResetSystem>().Won)
                {
                    horse.Direction = Utils.DegreeToVector(-Time.SecondsSinceStart*360);
                    if (!horse.IsJumping)
                        Jump(fam);
                }

                LerpTowardsTargetDirection(fam);

                if (!(horse.State == HorseState.Neutral && horse.StandingStill))
                {
                    fam.Physics.ForceToAdd += horse.Direction * horse.Speed * Time.FixedDeltaTime;
                }
                if(fam.Horse.Direction.Magnitude < .6f)
                {
                    fam.Physics.ForceToAdd += -fam.Physics.Velocity* 10 * Time.FixedDeltaTime;
                    
                }
            }

            foreach (var fam in World.Components.GetByFamily<BulletFam>())
            {
                if (!fam.Bullet.Landed && fam.Physics.Velocity.SquaredMagnitude < 0.001f)
                    OnSugarCubeLand(fam);
            }
        }


        private bool CheckJumpNeeded(HorseFam horsefam)
        {
            var dir = horsefam.Horse.Direction;
            return CheckCollidingWithJumpable(horsefam, dir);
        }

        private bool CheckCollidingWithJumpable(HorseFam horsefam, Vector2 offset = default)
        {
            foreach (var fam in World.Components.GetByFamily<JumpableFam>())
            {
                if (AABBCollisionTest.Test(fam.Transform.GlobalPosition, fam.Transform.LocalSize, horsefam.Transform.LocalPosition, horsefam.Transform.LocalSize + offset * .5f))
                {
                    return true;
                }
            }

            return false;
        }

        private void StopJump(HorseFam fam)
        {
            fam.Horse.TimeJumping = 0;
            fam.Horse.IsJumping = false;
            fam.Collider.ToIgnore = new HashSet<BoxColliderComponent>();
        }

        private void Jump(HorseFam fam)
        {
            fam.Horse.IsJumping = true;
            var toignorelist = new List<BoxColliderComponent>();
            toignorelist.AddRange(World.Components.GetByFamily<JumpableFam>().Select(f => f.Collider));
            toignorelist.AddRange(World.Components.GetByFamily<HiddenWallFam>().Select(f => f.Collider));
            fam.Collider.ToIgnore = new HashSet<BoxColliderComponent>(toignorelist);
        }


        private void Tamed(HorseFam fam, HorseData horse)
        {
            var tamedDir = Vector2.Zero;
            foreach (var key in horse.MovementKeys)
            {
                if (key.Certainty > Utils.RandomFloat() && Input.IsKeyHeld(key.Key))
                    tamedDir += key.Direction * key.Certainty;
            }
            if (Input.IsKeyPressed(horse.JumpKey.Key) && !horse.IsJumping)
            {
                if (horse.JumpKey.Certainty > .92f || horse.JumpKey.Certainty > Utils.RandomFloat())
                    Jump(fam);
            }
            horse.TargetDirection = tamedDir;
            horse.Speed = Utils.Lerp(HorseData.NeutralSpeed, HorseData.ExcitedSpeed, AverageCertainty(horse));

            if (horse.IsJumping)
                horse.Speed *= 1 + (horse.JumpKey.Certainty / 2f);
            
            CheckTamedOrNeutral(fam);
            CheckOverfed(horse);
        }

        private static void CheckTamedOrNeutral(HorseFam fam)
        {
            float averageCertainty = AverageCertainty(fam.Horse);

            if (averageCertainty > Utils.RandomFloat())
            {
                fam.Horse.State = HorseState.Tamed;
            }
            else
            {
                fam.Horse.State = HorseState.Neutral;
                fam.Horse.IdleTarget = RandomIdleTarget(fam.Transform.GlobalPosition);
            }
        }

        private static float AverageCertainty(HorseData horse)
        {
            return horse.MovementKeys.Average(k => k.Certainty);
        }

        private void Neutral(HorseFam fam, HorseData horse)
        {
            horse.Speed = HorseData.NeutralSpeed / Utils.Max(1, horse.FeedLevel);


            bool isAtTarget = horse.IdleTarget.HasValue && Utils.Distance(horse.IdleTarget.Value, fam.Transform.LocalPosition) < .5f;

            if (horse.IdleTarget.HasValue && Utils.Distance(horse.IdleTarget.Value, fam.Transform.LocalPosition) > 3)
            {
                horse.IdleTarget = RandomIdleTarget(fam.Transform.LocalPosition);
            }

            if (horse.IdleTarget.HasValue)
            {
                if (AverageCertainty(horse) < Utils.RandomFloat())
                    horse.TargetDirection = (horse.IdleTarget.Value - fam.Transform.LocalPosition).Normalized;

                if (isAtTarget)
                {
                    horse.IdleTarget = null;
                    horse.StandingStill = true;
                }
            }
            else if (Utils.RandomInt(0, 60) == 1)
            {
                //horse.IdleTarget = RandomIdleTarget();
                horse.StandingStill = false;
            }



            CheckTamedOrNeutral(fam);
            CheckOverfed(horse);
        }

        private void MoveTowardsSugarCube(HorseFam family)
        {

            var cubes = World.Components.GetByFamily<BulletFam>().Where(b => b.Bullet.Landed && !b.Bullet.TooOld).OrderBy(b => Utils.Distance(family.Transform.GlobalPosition, b.Transform.GlobalPosition));
            var nearest = cubes.FirstOrDefault();
            if (nearest.Transform == null)
            {
                family.Horse.State = HorseState.Neutral;
                return;
            }

            var delta = (nearest.Transform.GlobalPosition - (family.Transform.GlobalPosition + family.Collider.offset));
            var dir = delta.Normalized;
            family.Horse.TargetDirection = dir;
            family.Horse.Speed = HorseData.ExcitedSpeed / Utils.Max(1, family.Horse.FeedLevel);

            foreach (var key in family.Horse.MovementKeys)
            {
                if (Input.IsKeyHeld(key.Key))
                {
                    Vector2 keyDir = key.Direction;
                    Vector2 horseDir = family.Horse.Direction;

                    key.Direction = Utils.Lerp(keyDir, horseDir, Time.FixedDeltaTime);

                    if (DirectionDifferenceInDegrees(keyDir, horseDir) < 50)
                        key.Certainty = Utils.Lerp(key.Certainty, 1f, Time.FixedDeltaTime);
                    else
                        key.Certainty = Utils.Lerp(key.Certainty, 0, Time.FixedDeltaTime);

                }
            }

            if (Input.IsKeyHeld(family.Horse.JumpKey.Key))
            {
                var key = family.Horse.JumpKey;
                if (family.Horse.IsJumping && family.Horse.TimeJumping != HorseData.ExactTimeOnThing)
                    key.Certainty = Utils.Lerp(key.Certainty, 1f, Time.FixedDeltaTime);
                else
                    key.Certainty = Utils.Lerp(key.Certainty, 0, Time.FixedDeltaTime / 3);

            }


            if (delta.SquaredMagnitude < 0.5f)
            {
                World.Components.RemoveAllOf(nearest.Bullet.EntityID);
                family.Horse.FeedLevel += HorseData.FedFromSugarCube;
            }

            if (family.Horse.TimeInCurrentState > HorseData.ChaseGiveUpTime)
            {
                family.Horse.State = HorseState.Neutral;
                nearest.Bullet.TooOld = true;
                nearest.TextureRender.Tint.A = 0.5f;
            }

            CheckOverfed(family.Horse);
        }

        private static void CheckOverfed(HorseData horse)
        {
            //if (horse.FeedLevel > HorseData.FeedLevelToBeSick)
            //{
            //    horse.State = HorseState.Overfed;
            //    horse.StandingStill = false;
            //}

            horse.FeedLevel = Utils.Max(0, horse.FeedLevel - Time.FixedDeltaTime);
        }

        private static Vector2 RandomIdleTarget(Vector2 horsePos)
        {
            return horsePos + Utils.RandomPointInUnitCircle().Normalized * Utils.RandomFloat(1, 2);
        }


        private static float DirectionDifferenceInDegrees(Vector2 keyDir, Vector2 horseDir)
        {
            return Utils.Difference(Utils.VectorToDegree(keyDir) % 360f, Utils.VectorToDegree(horseDir) % 360f);
        }

        private static void LerpTowardsTargetDirection(HorseFam fam)
        {
            float a = Utils.VectorToDegree(fam.Horse.Direction);
            float b = Utils.VectorToDegree(fam.Horse.TargetDirection);
            float c = Time.FixedDeltaTime * 4 * fam.Horse.Speed;
            if (fam.Horse.TargetDirection == default)
                fam.Horse.Direction *= .9f;
            else
            {
                Vector2 normalized = Utils.DegreeToVector(Utils.LerpAngleDegrees(a, b, c)).Normalized;
                fam.Horse.Direction = normalized;

            }
        }

        private void OnSugarCubeLand(BulletFam bulletfam)
        {
            bulletfam.Bullet.Landed = true;
            foreach (var horseFam in World.Components.GetByFamily<HorseFam>())
                horseFam.Horse.State = HorseState.Excited;

            //World.Components.RemoveAllOf(bulletfam.Bullet.EntityID);
            //foreach (var horseFam in World.Components.GetByFamily<HorseFam>())
            //{
            //    bool shotCloseToHorse = Utils.Distance(horseFam.Transform.LocalCenterPoint, bulletfam.Transform.LocalCenterPoint) < 5;

            //    if (shotCloseToHorse)
            //    {
            //        horseFam.Horse.Anxiety += HorseData.AnxietyFromShot;
            //        horseFam.Horse.LastShotPosition = bulletfam.Transform.LocalCenterPoint;
            //        horseFam.Horse.LastShotTime = Time.SecondsSinceStart;
            //    }
            //}
        }
    }
}
