﻿using Nurose.Console;
using Nurose.Core;

namespace GMTK2020
{
    public class ShootingSystem : WorldSystem
    {
        public struct Fam
        {
            public Transform Transform;
            public ShooterData Shooter;
        }

        public void FixedUpdate()
        {
            if (World.Systems.Get<UISystem>().Help)
                return;

            foreach (var fam in World.Components.GetByFamily<Fam>())
            {

                if (Input.IsButtonPressed(MouseButton.Left) && fam.Transform.LocalCenterPoint.X < 450f / 32f)
                {
                    var pos = fam.Transform.LocalCenterPoint;
                    var target = World.RenderTarget.ScreenToWorld(Input.MousePosition);


                    var dir = (target - pos).Normalized;
                    target = pos + dir * 5;
                    BulletPrefab.Create(World, pos, target);
                }
            }
        }
    }
}
