﻿using Nurose.Core;
using System;
using System.Linq;

namespace GMTK2020
{
    public class CollisionSystem : WorldSystem
    {
        struct CollisionFam
        {
            public Transform Transform;
            public BoxColliderComponent BoxCollider;

            public Vector2 Center()
            {
                return Transform.LocalCenterPoint;
            }
        }

        public void FixedUpdate()
        {
            if (World.Systems.Get<UISystem>().Help)
                return;

            var fams = World.Components.GetByFamily<CollisionFam>().Where(f => f.BoxCollider.IsEnabled).ToArray();

            for (int i = 0; i < fams.Length; i++)
            {
                for (int j = i + 1; j < fams.Length; j++)
                {

                    var a = fams[i];
                    var b = fams[j];

                    if (CheckColliding(a, b))
                        Collide(a, b);
                }
            }

        }

        private void Collide(CollisionFam a, CollisionFam b)
        {
            a.BoxCollider.IsColliding = true;
            b.BoxCollider.IsColliding = true;

            a.BoxCollider.CollidingWith = b.BoxCollider;
            b.BoxCollider.CollidingWith = a.BoxCollider;

            HorseData horseData = World.Components.Get<HorseData>(b.BoxCollider.EntityID);
            ResetOnCollisionComponent resetOnCollisionComponent = World.Components.Get<ResetOnCollisionComponent>(a.BoxCollider.EntityID);

            if (horseData != null && resetOnCollisionComponent != null)
                World.Systems.Get<GameResetSystem>().ResetWorld();

            ResolveCollision(a, b);
        }

        private void ResolveCollision(CollisionFam a, CollisionFam b)
        {
            while (CheckColliding(a, b) && (!a.BoxCollider.IsStatic || !b.BoxCollider.IsStatic))
            {
                var dir = (a.Center() - b.Center()).Normalized;

                if (dir == default)
                    dir = Utils.RandomPointInUnitCircle();

                a.Transform.LocalPosition += dir * .01f * (a.BoxCollider.IsStatic ? 0 : 1);
                b.Transform.LocalPosition -= dir * .01f * (b.BoxCollider.IsStatic ? 0 : 1);
            }
        }

        private bool CheckColliding(CollisionFam a, CollisionFam b)
        {
            if ((a.BoxCollider.ToIgnore?.Contains(b.BoxCollider) ?? false) || (b.BoxCollider.ToIgnore?.Contains(a.BoxCollider) ?? false)) return false;

            Vector2 localPosition = a.Transform.LocalPosition + a.BoxCollider.offset;
            Vector2 localSize = a.Transform.LocalSize * a.BoxCollider.Size;

            Vector2 localPosition1 = b.Transform.LocalPosition + b.BoxCollider.offset;
            Vector2 localSize1 = b.Transform.LocalSize * b.BoxCollider.Size;
            return AABBCollisionTest.Test(localPosition, localSize, localPosition1, localSize1);
        }
    }
}
