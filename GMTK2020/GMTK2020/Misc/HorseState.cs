﻿namespace GMTK2020
{

    public enum HorseState
    {
        Neutral,
        Excited,
        Tamed,
    }
}