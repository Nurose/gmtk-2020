﻿using Nurose.Core;

namespace GMTK2020
{

    public class LearnedKey
    {
        public Key Key;
        public Vector2 Direction;
        public float Certainty;

        public LearnedKey(Key key)
        {
            Key = key;
        }
    }
}