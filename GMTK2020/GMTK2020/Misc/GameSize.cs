﻿using Nurose.Core;

namespace GMTK2020
{
    public static class GameSize
    {
        public static Vector2Int Size = (1000, 800);
        public static int PixelsPerUnit = 32;
    }
}
