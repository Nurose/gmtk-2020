﻿using Nurose.Core;
using Nurose.Text;

namespace GMTK2020
{
    public static class DrawerExtentions
    {
        public static UnitRectangleDrawTask rectTask = new UnitRectangleDrawTask(Color.White, 1, false);
        private static VertexBuffer Outline { get; set; }
        private static TextDrawer textDrawer { get; set; } = new TextDrawer();
        private static ResourceManager ResourceManager { get; set; } = new ResourceManager();

        public static void DrawRect(this Drawer drawer, Vector2Int position, Vector2 size, Color color, float depth, Texture texture = null)
        {
            if (texture == null)
                drawer.SetTexture(Texture.White1x1);
            else
                drawer.SetTexture(texture);

            drawer.Draw(new ArbitraryRenderTask((rt) =>
            {
                rectTask.Color = color;
                rectTask.Depth = depth;
                rt.ModelMatrix = new TransformData(position, size).CalcModelMatrix();
                rectTask.Execute(rt);
            }));
        }

        public static int GetTextWidth(this Drawer drawer, string Text, int lineheight, ResourceManager resources, string fontName)
        {
            textDrawer.Text = Text;
            textDrawer.BMFont = resources[fontName] as BMFont;
            return textDrawer.CalcTextWidth(lineheight);
        }
        public static void StartPixelSpace(this Drawer drawer)
        {
            drawer.Draw(new StoreMatrixes(ResourceManager, "world"));
            drawer.Draw(new SetPixelCoordinates());
        }

        public static void EndPixelSpace(this Drawer drawer)
        {
            drawer.Draw(new LoadStoredMatricies(ResourceManager, "world"));
        }


        public static void DrawRectWireFrames(this Drawer drawer, Vector2 position, Vector2 size, Color color, float depth, int widht = 1)
        {
            drawer.SetTexture(Texture.White1x1);
            drawer.Draw(new ArbitraryRenderTask((rt) =>
            {
                rectTask.Color = color;
                rectTask.Depth = depth;
                rt.ModelMatrix = new TransformData(position, (widht, size.Y)).CalcModelMatrix();
                rectTask.Execute(rt);
                rt.ModelMatrix = new TransformData(position, (size.X, widht)).CalcModelMatrix();
                rectTask.Execute(rt);
                rt.ModelMatrix = new TransformData(position + (size.X - widht, 0), (widht, size.Y)).CalcModelMatrix();
                rectTask.Execute(rt);
                rt.ModelMatrix = new TransformData(position + (0, size.Y - widht), (size.X, widht)).CalcModelMatrix();
                rectTask.Execute(rt);
            }));
        }

        public static void DrawRectWireFramesWithoutBottem(this Drawer drawer, Vector2 position, Vector2 size, Color color, float depth, int widht = 1)
        {
            drawer.SetTexture(Texture.White1x1);
            drawer.Draw(new ArbitraryRenderTask((rt) =>
            {
                rectTask.Color = color;
                rectTask.Depth = depth;
                rt.ModelMatrix = new TransformData(position, (widht, size.Y)).CalcModelMatrix();
                rectTask.Execute(rt);
                rt.ModelMatrix = new TransformData(position, (size.X, widht)).CalcModelMatrix();
                rectTask.Execute(rt);
                rt.ModelMatrix = new TransformData(position + (size.X - widht, 0), (widht, size.Y + widht)).CalcModelMatrix();
                rectTask.Execute(rt);
                // rt.ModelMatrix = new TransformData(position + (0, size.Y), (size.X, widht)).CalcModelMatrix();
                // rectTask.Execute(rt);
            }));
        }

        public static void DrawText(this Drawer drawer, string text, Vector2Int position, int lineheight, Color color, float depth, ResourceManager resources, string fontName, bool centered = false)
        {
            drawer.SetModelMatrix(new TransformData(position, new Vector2(lineheight, lineheight)).CalcModelMatrix());
            drawer.Draw((rt =>
            {
                textDrawer.BMFont = resources[fontName] as BMFont;
                textDrawer.Text = text;
                textDrawer.Centered = centered;
                textDrawer.Color = color;
                textDrawer.InvertedY = false;
                textDrawer.ScaleStyle = ScaleStyle.ForceLineHeight;
                textDrawer.Depth = depth;
                textDrawer.Text = text;
                textDrawer.Execute(rt);
            }));
        }
    }
}