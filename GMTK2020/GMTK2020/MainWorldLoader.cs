﻿using Nurose.Console;
using Nurose.Core;
using Nurose.Text;
using System.Drawing;
using System.Drawing.Imaging;
using Color = Nurose.Core.Color;

namespace GMTK2020
{
    internal static class MainWorldLoader
    {

        static Vector2 ImagePosToWorldPos(Vector2Int pos)
        {
            return new Vector2(pos.X, 1000 - pos.Y) / GameSize.PixelsPerUnit;
        }

        internal static void Load(World world)
        {
            world.InputRefreshOption = InputRefreshOption.FixedUpdate;

            world.ResourceManager.Hold("mainbackgroundTexture", new Texture("Resources/Images/DecoratedBackground.png"));
            world.ResourceManager.Hold("bushTexture", new Texture("Resources/Images/Bush.png"));
            world.ResourceManager.Hold("lowbushTexture", new Texture("Resources/Images/LowBush.png"));
            world.ResourceManager.Hold("pawnTexture", new Texture("Resources/Images/Pawn.png"));
            world.ResourceManager.Hold("sugarCubeTexture", new Texture("Resources/Images/SugarCube.png"));
            world.ResourceManager.Hold("wasd", new Texture("Resources/Images/wasd.png"));
            world.ResourceManager.Hold("space", new Texture("Resources/Images/space.png"));
            world.ResourceManager.Hold("calibri", new BMFont("Resources/Fonts/calibri.fnt"));
            

            NuroseMain.Window.ClearColor = new Color(144/255f,155/255f,68/255f); 
            BackgroundPrefab.Create(world);
            SpawnObstacles(world);
            
            PlayerPrefab.Create(world);
            CameraPrefab.Create(world);


            world.Systems.Add<MovementSystem>();
            world.Systems.Add<ShootingSystem>();
            world.Systems.Add<CollisionSystem>();
            world.Systems.Add<PhysicsSystem>();
            world.Systems.Add<GameResetSystem>();
            world.Systems.Add<ConsoleCommandsSystem>();
            world.Systems.Add<ParticleSystem>();
            world.Systems.Add<CameraSystem>();

            world.Systems.Add<RenderSystem>();
            world.Systems.Add<UISystem>();
            world.Systems.Add<ConsoleSystem>();
        }

        private static void SpawnObstacles(World world)
        {
            using (Bitmap bitmap = new Bitmap("Resources/Images/MainBackground.png"))
            {
                var data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, global::System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                unsafe
                {
                    byte* scan0 = (byte*)data.Scan0.ToPointer();

                    for (int x = 0; x < bitmap.Width; x++)
                    {
                        for (int y = 0; y < bitmap.Height; y++)
                        {
                            int i = (y * bitmap.Width + x) * 4;
                            var col = Color.FromBytes(scan0[i + 2], scan0[i + 1], scan0[i]);

                            if (col == Color.Black)
                            {
                                BushPrefab.Create(world, ImagePosToWorldPos((x, y)) - Vector2.One / 2);
                            }
                            else if ((int)(col.R * 255) == 255 && (int)(col.G * 255) == 125)
                            {
                                PawnPrefab.Create(world, ImagePosToWorldPos((x, y)) - Vector2.One / 2.8f);
                            }
                            else if (col == new Color(0,255,255))
                            {
                                JumpablePrefab.Create(world, ImagePosToWorldPos((x, y)) - Vector2.One / 2);
                            }
                            else if (col ==  Color.FromBytes(1, 1, 255))
                            {
                                HiddenWallPrefab.Create(world, ImagePosToWorldPos((x, y)) - Vector2.One / 4);
                            }
                        }
                    }
                }
            }
        }
    }
}
