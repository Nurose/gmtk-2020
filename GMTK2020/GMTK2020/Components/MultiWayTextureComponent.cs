﻿using Nurose.Core;
using System.Collections.Generic;

namespace GMTK2020
{
    public class MultiWayTextureComponent : Component
    {
        public class Entry
        {
            public Vector2 dir;
            public Texture Texture;

            public Entry(Vector2 dir, Texture texture)
            {
                this.dir = dir.Normalized;
                Texture = texture;
            }
        }

        public Entry Current ;
        public List<Entry> Entries;
    }
}