﻿using Nurose.Core;

namespace GMTK2020
{
    public class Particle : Component
    {
        public Color Color = Color.Red;
        public Vector2 Velocity;

        public float TimeAlive;
    }
}