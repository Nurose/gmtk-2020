﻿using Nurose.Core;

namespace GMTK2020
{

    public class BulletComponent : Component
    {
        public static float Speed = 10;
        public Vector2 StartDirection;
        public Vector2 Target;
        public bool Landed;
        public bool TooOld;
    }
}