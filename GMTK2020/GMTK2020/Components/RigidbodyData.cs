﻿using Nurose.Core;

namespace GMTK2020
{
    public class PhysicsData : Component
    {
        public Vector2 Velocity;
        public Vector2 ForceToAdd;

        public void AddForce(Vector2 force)
        {
            ForceToAdd += force;
        }
    }
}