﻿using Nurose.Core;

namespace GMTK2020
{
    public class TextureRenderData : Component
    {
        public Texture Texture;
        public Color Tint = Color.White;
    }
}
