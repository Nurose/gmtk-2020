﻿using Nurose.Core;

namespace GMTK2020
{

    public class HorseData : Component
    {
        public const float NeutralSpeed = 1;
        public const float OverfedSpeed = .01f;
        public const float ExcitedSpeed = 5;
        public const float FedFromSugarCube = 1.5f;
        public const float ChaseGiveUpTime = 6f;
        public const float JumpTime = .5f;
        public const float ExactTimeOnThing = JumpTime / 4 * 3;

        public static readonly Vector2 SpawnPos = new Vector2(290, 1000 - 370) / GameSize.PixelsPerUnit;

        public LearnedKey[] MovementKeys;
        public LearnedKey JumpKey;

        public bool StandingStill;
        public float LastShotTime;
        public float Speed;
        public HorseState State;
        public float FeedLevel = 0;
        public float TimeInCurrentState = 0;
        public Vector2 Direction = Vector2.One.Normalized;
        public Vector2 TargetDirection = Vector2.One.Normalized;
        public Vector2? IdleTarget;

        public bool IsJumping;
        public float OnThing;
        public float TimeJumping;
    }
}