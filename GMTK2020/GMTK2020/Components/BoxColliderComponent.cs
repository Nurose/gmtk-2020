﻿using Nurose.Core;
using System.Collections.Generic;

namespace GMTK2020
{
    public class BoxColliderComponent : Component
    {
        public bool IsEnabled = true;
        public bool IsColliding;
        public bool IsStatic;
        public BoxColliderComponent CollidingWith;
        public HashSet<BoxColliderComponent> ToIgnore;
        public Vector2 offset = Vector2.Zero;
        public Vector2 Size = Vector2.One;
    }
}