﻿using Nurose.Console;
using Nurose.Core;
using Nurose.OpenTK;
using System;

namespace GMTK2020
{
    class Program
    {
        static void Main(string[] args)
        {
            NuroseMain.SetWindowFactory<OpenTKWindowFactory>();
            NuroseMain.SetGraphicsFactory<OpenTKGraphicsFactory>();
            NuroseMain.Create("Untamed Horse", GameSize.Size, 90);
            MainWorldLoader.Load(NuroseMain.World);
            
            NuroseMain.Start();
        }
    }
}
